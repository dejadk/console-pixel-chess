![Pixel Chess Valid Movement Indicator](Demo/PixelChessMoveIndicator.gif)

This was a project I created to help teach my son chess. It's displayed in the console and uses unicode characters as "pixels". Movement is made with left mouse clicks.

It's currently in the middle of a refactor. I still need to add movement validation for knight pieces as well as win conditions and special rules like castling, promotion, and en passant.