#pragma once
#include <stdbool.h>
#include "pixel.h"
#include "console.h"

#define SQUARES_PER_ROW 8
#define SQUARES_PER_COLUMN 8
#define SQUARE_WIDTH 14
#define SQUARE_HEIGHT 7
#define TOTAL_SQUARES 64

#define PIXELS_PER_CELL (SQUARE_WIDTH * SQUARE_HEIGHT)
#define PIXELS_PER_ROW (SQUARE_WIDTH * SQUARES_PER_ROW)
#define PIXELS_PER_COL (SQUARE_HEIGHT * SQUARES_PER_COLUMN)
#define TOTAL_PIXELS (PIXELS_PER_CELL * TOTAL_SQUARES)

#define BOARD_TOPMOST ((WINDOW_HEIGHT - PIXELS_PER_COL) / 2)
#define BOARD_LEFTMOST ((WINDOW_WIDTH - PIXELS_PER_ROW) / 2)
#define BOARD_RIGHTMOST (BOARD_LEFTMOST + PIXELS_PER_ROW)
#define BOARD_BOTTOMMOST (BOARD_TOPMOST + PIXELS_PER_COL)

#define LIGHT_SQUARE_COLOR kLightGray
#define DARK_SQUARE_COLOR kDarkGray

#define TOTAL_PAWN_PIXELS 18
#define TOTAL_ROOK_PIXELS 46
#define TOTAL_BISHOP_PIXELS 34
#define TOTAL_KNIGHT_PIXELS 47
#define TOTAL_QUEEN_PIXELS 44
#define TOTAL_KING_PIXELS 44

#define LIGHT_PIECE_COLOR kWhite
#define DARK_PIECE_COLOR kBlack

#define MAX_PIECE_MOVES 28
#define PAWN_MAX_DIAG_MOVE_DIST 1
#define PAWN_MAX_STRAIGHT_MOVE_DIST 2
#define ROOK_MAX_MOVE_DIST 7
#define BISHOP_MAX_MOVE_DIST 7
#define QUEEN_MAX_MOVE_DIST 7
#define KING_MAX_MOVE_DIST 1
#define LIGHT_PAWN_STARTING_ROW 6
#define DARK_PAWN_STARTING_ROW 1

typedef enum _Type {
  kEmpty,
  kPawn,
  kKnight,
  kBishop,
  kRook,
  kQueen,
  kKing
} Type;

typedef enum _Color { 
  kNone,
  kLight,
  kDark
} Color;

typedef struct _Position {
  unsigned int column;
  unsigned int row;
} Position;

typedef struct _Piece {
  Type type;
  Color color;
  Sprite sprite;
} Piece;

typedef struct _Square {
  Position position;
  Color color;
  Piece piece;
} Square;

typedef struct _Board {
  Square square[SQUARES_PER_COLUMN][SQUARES_PER_ROW];
} Board;

typedef struct _Player {
  wchar_t* name;
} Player;

typedef struct _Chess {
  Player player_light;
  Player player_dark;
  Board board;
  Color turn;
} Chess;

Pixel pawn_pixels[];
Pixel rook_pixels[];
Pixel bishop_pixels[];
Pixel knight_pixels[];
Pixel queen_pixels[];
Pixel king_pixels[];
Pixel circle_pixels[];

Sprite empty_sprite;
Sprite pawn_sprite;
Sprite rook_sprite;
Sprite bishop_sprite;
Sprite knight_sprite;
Sprite queen_sprite;
Sprite king_sprite;
Sprite circle_sprite;
Sprite square_sprite;

bool printing_moves;

void print_board(Board* board);
void refresh_board(Board board, Square* squares_to_refresh, unsigned int num_of_pos);
void start_game(Chess* game);
typedef void(*fp_mouse_callback)(Board*, Color, Square*, COORD*);