#include "pixel.h"
#include "letters.h"

Sprite letter_a = {TOTAL_LETTER_A_PIXELS, LETTER_WIDTH, LETTER_HEIGHT, false, kWhite, kWhite};

// This was a test for printing characters in a pixel format. I'll probably simplify this and just print ASCII text instead.
Pixel letter_a_pixels[] = {
  {{3, 0}, PIXEL_LOWER_HALF, 0, 0}, 
  {{4, 0}, PIXEL_LOWER_HALF, 0, 0},
  {{5, 0}, PIXEL_LOWER_HALF, 0, 0},
  {{6, 0}, PIXEL_LOWER_HALF, 0, 0},
  {{2, 1}, PIXEL_RIGHT_HALF, 0, 0},
  {{7, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{2, 2}, PIXEL_RIGHT_HALF, 0, 0},
  {{3, 2}, PIXEL_UPPER_HALF, 0, 0},
  {{4, 2}, PIXEL_UPPER_HALF, 0, 0},
  {{5, 2}, PIXEL_UPPER_HALF, 0, 0},
  {{6, 2}, PIXEL_UPPER_HALF, 0, 0},
  {{7, 2}, PIXEL_LEFT_HALF, 0, 0},
  {{2, 3}, PIXEL_RIGHT_HALF, 0, 0},
  {{7, 3}, PIXEL_LEFT_HALF, 0, 0}
};

void init_letters() { 
  init_detailed_sprite(&letter_a, letter_a_pixels); 
}