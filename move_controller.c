#include <stdbool.h>
#include "pixel.h"
#include "move_controller.h"
#include "chess.h"

Square valid_moves[MAX_PIECE_MOVES];
unsigned int _num_of_valid_moves = 0;
unsigned int* num_of_valid_moves = &_num_of_valid_moves;

// Forward declare private functions.
// These are not exposted in move_controller.h
bool is_valid_move(Piece src_piece, Square dest_square, bool* is_path_blocked, bool is_move_diagonal);
void add_valid_move(Square valid_move);
void move_piece(Board* board, Square source, Square dest);
void handle_pawn(Board board, Square square);
void handle_rook(Board board, Square square);
void handle_knight(Board board, Square square);
void handle_bishop(Board board, Square square);
void handle_queen(Board board, Square square);
void handle_king(Board board, Square square);
void get_diagonal_moves(Board board, Square selected_square, unsigned int dist);
void get_straight_moves(Board board, Square selected_square, unsigned int dist);
void validate_straight_dest(Position position, unsigned int dist_index, StraightPath* straight_path);
void validate_diagonal_dest(Position position, unsigned int dist_index, DiagonalPath* diagonal_path);
bool is_straight_path_blocked(StraightPath straight_path);
bool is_diagonal_path_blocked(DiagonalPath diagonal_path);

void print_valid_moves() {
  COORD move_indicator_pos = {0, 0};

  for (size_t i = 0; i < *num_of_valid_moves; i++) {
    move_indicator_pos.X = (valid_moves[i].position.row * SQUARE_WIDTH) + BOARD_LEFTMOST;
    move_indicator_pos.Y = (valid_moves[i].position.column * SQUARE_HEIGHT) + BOARD_TOPMOST;
    print_sprite(&circle_sprite, move_indicator_pos);
  }
}

void get_diagonal_moves(Board board, Square selected_square, unsigned int dist) {
  Square dest_square;
  Piece selected_piece = selected_square.piece;
  DiagonalPath diagonal_path = {.is_top_left_blocked = false, .is_top_right_blocked = false, .is_bottom_left_blocked = false, .is_bottom_right_blocked = false,};
  bool is_move_diagonal = true;

  for (size_t i = 1; i <= dist; i++) {
    validate_diagonal_dest(selected_square.position, i, &diagonal_path);

    // Check if we're at the end of the top left diagonal line.
    if (!diagonal_path.is_top_left_blocked) {
      dest_square = board.square[selected_square.position.column - i][selected_square.position.row - i];
      if (is_valid_move(selected_piece, dest_square, &diagonal_path.is_top_left_blocked, is_move_diagonal)) {
        add_valid_move(dest_square);
      }
    }
    // Check if we're at the end of the top right diagonal line.
    if (!diagonal_path.is_top_right_blocked) {
      dest_square = board.square[selected_square.position.column - i][selected_square.position.row + i];
      if (is_valid_move(selected_piece, dest_square, &diagonal_path.is_top_right_blocked, is_move_diagonal)) {
        add_valid_move(dest_square);
      }
    }
    // Check if we're at the end of the bottom left diagonal line.
    if (!diagonal_path.is_bottom_left_blocked) {
      dest_square = board.square[selected_square.position.column + i][selected_square.position.row - i];
      if (is_valid_move(selected_piece, dest_square, &diagonal_path.is_bottom_left_blocked, is_move_diagonal)) {
        add_valid_move(dest_square);
      }
    }
    // Check if we're at the end of the bottom right diagonal line.
    if (!diagonal_path.is_bottom_right_blocked) {
      dest_square = board.square[selected_square.position.column + i][selected_square.position.row + i];
      if (is_valid_move(selected_piece, dest_square, &diagonal_path.is_bottom_right_blocked, is_move_diagonal)) {
        add_valid_move(dest_square);
      }
    }

    if (is_diagonal_path_blocked(diagonal_path)) {
      return;
    }
  }
}

void get_straight_moves(Board board, Square selected_square, unsigned int dist) {
  Square dest_square;
  Piece selected_piece = selected_square.piece;
  StraightPath straight_path = {.is_left_blocked = false,
                                .is_right_blocked = false,
                                .is_top_blocked = false,
                                .is_bottom_blocked = false};
  bool is_move_diagonal = false;

  // Pawns can only move straight in one direction.
  // Determine which direction is "forward" relative to the current player's side.
  if (selected_piece.type == kPawn) {
    straight_path.is_left_blocked = true;
    straight_path.is_right_blocked = true;
    if (selected_piece.color == kLight) {
      straight_path.is_bottom_blocked = true;
    } else {
      straight_path.is_top_blocked = true;
    }
  }

  for (size_t i = 1; i <= dist; i++) {
    validate_straight_dest(selected_square.position, i, &straight_path);

    if (!straight_path.is_left_blocked) {
      dest_square = board.square[selected_square.position.column][selected_square.position.row - i];
      if (is_valid_move(selected_piece, dest_square, &straight_path.is_left_blocked, is_move_diagonal)) {
        add_valid_move(dest_square);
      }
    }
    if (!straight_path.is_right_blocked) {
      dest_square = board.square[selected_square.position.column][selected_square.position.row + i];
      if (is_valid_move(selected_piece, dest_square, &straight_path.is_right_blocked, is_move_diagonal)) {
        add_valid_move(dest_square);
      }
    }
    if (!straight_path.is_top_blocked) {
      dest_square = board.square[selected_square.position.column - i][selected_square.position.row ];
      if (is_valid_move(selected_piece, dest_square, &straight_path.is_top_blocked, is_move_diagonal)) {
        add_valid_move(dest_square);
      }
    }
    if (!straight_path.is_bottom_blocked) {
      dest_square = board.square[selected_square.position.column + i][selected_square.position.row];
      if (is_valid_move(selected_piece, dest_square, &straight_path.is_bottom_blocked, is_move_diagonal)) {
        add_valid_move(dest_square);
      }
    }
    if (is_straight_path_blocked(straight_path)) {
      return;
    }
  }
}

bool is_valid_move(Piece src_piece, Square dest_square, bool* is_path_blocked, bool is_move_diagonal) {
  // Check if we're within the game board still.
  if (dest_square.position.row >= 0 && 
      dest_square.position.row < SQUARES_PER_ROW && 
      dest_square.position.column >= 0 &&
      dest_square.position.column < SQUARES_PER_COLUMN) {
    // Check if the next square is either empty or not occupied by one of our
    // own pieces.
    if (dest_square.piece.color == kNone) {
      // Check if the origin_piece is a pawn.
      // If it's a pawn the only legal diagonal move is to capture an enemy.
      if (is_move_diagonal && src_piece.type == kPawn) {
        return false;
      } else {
        return true;
      }
      // If a square wasn't empty then it can only be one of our own or an enemy
      // piece. This will check if it's an enemy piece. This means it's a valid
      // move because you can capture the piece. It also needs to block any
      // future valid moves because you can't move through the enemy piece.
    } else if (dest_square.piece.color != src_piece.color) {
      if (!is_move_diagonal && src_piece.type == kPawn) {
        *is_path_blocked = true;
        return false;
      } else {
        *is_path_blocked = true;
        return true;
      }
    } else {
      // The next square was our own piece and we can't move through it so we
      // mark the top left diagonal line as "blocked".
        *is_path_blocked = true;
        return false;
    }
  } else {
    // We've hit the bounds if the chess board. Marking the top left diagonal
    // line as "blocked".
    *is_path_blocked = true;
    return false;
  }
}

void validate_diagonal_dest(Position position, unsigned int dist_index, DiagonalPath* diagonal_path) {
  if (position.column < dist_index) {
    diagonal_path->is_top_left_blocked = true;
    diagonal_path->is_top_right_blocked = true;
  }

  if (position.row < dist_index) {
    diagonal_path->is_bottom_left_blocked = true;
    diagonal_path->is_top_left_blocked = true;
  }

  if (position.column + dist_index > 7) {
    diagonal_path->is_bottom_left_blocked = true;
    diagonal_path->is_bottom_right_blocked = true;
  }

  if (position.row + dist_index > 7) {
    diagonal_path->is_top_right_blocked = true;
    diagonal_path->is_bottom_right_blocked = true;
  }
}

void validate_straight_dest(Position position, unsigned int dist_index, StraightPath* straight_path) { 
  if (position.column < dist_index) {
    straight_path->is_top_blocked = true;
  }

  if (position.row < dist_index) {
    straight_path->is_left_blocked = true;
  }

  if (position.column + dist_index > 7) {
    straight_path->is_bottom_blocked = true;
  }

  if (position.row + dist_index > 7) {
    straight_path->is_right_blocked = true;
  }
}

void add_valid_move(Square valid_move) {
  // When should num_of_valid_moves be reset? Not here...
  //*num_of_valid_moves = 0;
  valid_moves[*num_of_valid_moves] = valid_move;
  // This must be += 1 and NOT ++.
  // Using ++ here will use pointer math and increase the pointer rather than the value.
  *num_of_valid_moves += 1;
}

void reset_num_of_valid_moves() { 
  *num_of_valid_moves = 0; 
}

void generate_valid_moves(Board* board, Square square) {
  switch (square.piece.type) {
    case kPawn:
      handle_pawn(*board, square);
      break;

    case kRook:
      handle_rook(*board, square);
      break;

    case kKnight:
      handle_knight(*board, square);
      break;

    case kBishop:
      handle_bishop(*board, square);
      break;

    case kQueen:
      handle_queen(*board, square);
      break;

    case kKing:
      handle_king(*board, square);
      break;

    default :
      // TODO: Add error catching and put one here.
      break;
  }
}

bool valid_move_found(Square dest_square) {
  for (size_t i = 0; i < *num_of_valid_moves; i++) {
    if (dest_square.position.column == valid_moves[i].position.column && dest_square.position.row == valid_moves[i].position.row) {
      return true;
    }
  }
  return false;
}

void move_piece(Board* board, Square src, Square dest) {
  Square* src_square = &board->square[src.position.column][src.position.row];
  Square* dest_square = &board->square[dest.position.column][dest.position.row];
  Piece src_piece = board->square[src.position.column][src.position.row].piece;

  src_square->piece.color = kNone;
  src_square->piece.type = kEmpty;
  src_square->piece.sprite = empty_sprite;

  dest_square->piece.color = src_piece.color;
  dest_square->piece.type = src_piece.type;
  dest_square->piece.sprite = src_piece.sprite;
}

void handle_pawn(Board board, Square square) {
  get_diagonal_moves(board, square, PAWN_MAX_DIAG_MOVE_DIST);

  if (square.position.column == LIGHT_PAWN_STARTING_ROW || square.position.column == DARK_PAWN_STARTING_ROW) {
    get_straight_moves(board, square, PAWN_MAX_STRAIGHT_MOVE_DIST);
  } else {
    get_straight_moves(board, square, PAWN_MAX_STRAIGHT_MOVE_DIST - 1);
  }
}

void handle_rook(Board board, Square square) {
  get_straight_moves(board, square, ROOK_MAX_MOVE_DIST);
}

void handle_knight(Board board, Square square) {
  //get_L_shaped_moves();
}

void handle_bishop(Board board, Square square) {
  get_diagonal_moves(board, square, ROOK_MAX_MOVE_DIST);
}

void handle_queen(Board board, Square square) {
  get_straight_moves(board, square, QUEEN_MAX_MOVE_DIST);
  get_diagonal_moves(board, square, QUEEN_MAX_MOVE_DIST);
}

void handle_king(Board board, Square square) {
  get_straight_moves(board, square, KING_MAX_MOVE_DIST);
  get_diagonal_moves(board, square, KING_MAX_MOVE_DIST);
}

bool is_diagonal_path_blocked(DiagonalPath diagonal_path) {
  if (diagonal_path.is_bottom_left_blocked &&
      diagonal_path.is_bottom_right_blocked &&
      diagonal_path.is_top_left_blocked &&
      diagonal_path.is_top_right_blocked) {
    return true;
  }
  return false;
}

bool is_straight_path_blocked(StraightPath straight_path) {
  if (straight_path.is_right_blocked &&
      straight_path.is_left_blocked &&
      straight_path.is_top_blocked &&
      straight_path.is_bottom_blocked) {
    return true;
  }
  return false;
}