#pragma once
#include "chess.h"

#define CIRCLE_WIDTH 7
#define CIRCLE_HEIGHT 14
#define TOTAL_CIRCLE_PIXELS 10

Square valid_moves[];
unsigned int* num_of_valid_moves;

typedef struct _DiagonalPath {
  bool is_top_left_blocked;
  bool is_top_right_blocked;
  bool is_bottom_left_blocked;
  bool is_bottom_right_blocked;
} DiagonalPath;

typedef struct _StraightPath {
  bool is_left_blocked;
  bool is_right_blocked;
  bool is_top_blocked;
  bool is_bottom_blocked;
} StraightPath;

void print_valid_moves();
void reset_num_of_valid_moves();
void move_piece(Board* board, Square source, Square dest);
void generate_valid_moves(Board* board, Square square);
bool valid_move_found(Square dest_square);