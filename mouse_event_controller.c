#include <Windows.h>
#include "console.h"
#include "chess.h"
#include "mouse_event_controller.h"
#include "move_controller.h"

#define MOUSE_BUTTON_CLICKED 0
#define INPUT_BUFFER_SIZE 128

INPUT_RECORD input_buffer[INPUT_BUFFER_SIZE];
DWORD num_of_events_read;
COORD square_position;

// Forward declaring private functions.
// These are not exposted in mouse_event_controller.h
COORD screen_to_board(COORD position);

//
// FOR DEBUG PURPOSES ONLY
//
void clear_line(COORD position) {
  SetConsoleCursorPosition(hConsoleOut, position);
  for (size_t i = 0; i < BOARD_LEFTMOST - 10; i++) {
      wprintf(L" ");
  }
  SetConsoleCursorPosition(hConsoleOut, position);
}

// This needs to be constantly called inside a loop to always be looking for input.
void mouse_handler(Board* board, Color turn, Square* square, fp_mouse_callback callback) {
  COORD mouse_pos = {0, 0};
  COORD* ptr_mouse_pos = &mouse_pos;
    ReadConsoleInput(hConsoleIn, input_buffer, INPUT_BUFFER_SIZE,
                     &num_of_events_read);

    for (size_t i = 0; i < num_of_events_read; i++) {
      if (input_buffer[i].EventType == MOUSE_EVENT) {
        MOUSE_EVENT_RECORD mouse_event = input_buffer[i].Event.MouseEvent;
        // This will return whichever square was left clicked inside the game board.
        if (mouse_event.dwEventFlags == MOUSE_BUTTON_CLICKED &&
            mouse_event.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED &&
            mouse_event.dwMousePosition.X > BOARD_LEFTMOST &&
            mouse_event.dwMousePosition.X < BOARD_RIGHTMOST &&
            mouse_event.dwMousePosition.Y > BOARD_TOPMOST &&
            mouse_event.dwMousePosition.Y < BOARD_BOTTOMMOST) {
          *ptr_mouse_pos = mouse_event.dwMousePosition;
          callback(board, turn, square, ptr_mouse_pos);
        }
      }
    }
}