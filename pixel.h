﻿#pragma once
#include <Windows.h>
#include <wchar.h>
#include <stdbool.h>

#define PIXEL_LIGHT_SHADE L'\u2591'   // ░
#define PIXEL_FULL_BLOCK L'\u2588'    // █
#define PIXEL_UPPER_HALF L'\u2580'    // ▀
#define PIXEL_LOWER_HALF L'\u2584'    // ▄
#define PIXEL_LEFT_HALF L'\u258C'     // ▌
#define PIXEL_RIGHT_HALF L'\u2590'    //  ▐

typedef enum _Colors {
  kBlack,
  kBlue,
  kBreen,
  kAqua,
  kRed,
  kPurple,
  kYellow,
  kLightGray,
  kDarkGray,
  kLightBlue,
  kLightGreen,
  kLightAqua,
  kLightRed,
  kLightPurple,
  kLightYellow,
  kWhite
} Colors;

typedef struct _Pixel {
  COORD position;
  wint_t pixel_char;
  byte foreground_color;
  byte background_color;
} Pixel;

// total_pixels is how many pixels are being used in the sprite.
// total_pixels should match how many Pixel pointers you need.
// When unique_pixel_colors is true Sprites will be printed using each Pixel's individual color value.
// When unique_pixel_colors is false the Sprite will be printed using the colors defined in the struct Sprite only.
typedef struct _Sprite {
  short total_pixels;
  short width;
  short height;
  bool unique_pixel_colors;
  byte foreground_color;
  byte background_color;
  Pixel* pixels;
} Sprite;

void print_all_colors();
void print_newline();
void print_pixel(wint_t pixel);
void init_detailed_sprite(Sprite* sprite, Pixel pixel_arr[]);
void init_generic_sprite(Sprite* sprite, wint_t pixel_char, byte foreground_color, byte background_color);
void print_sprite(Sprite* sprite, COORD screen_offset);
void destroy_sprite(Sprite sprite);
byte get_color(byte foreground_color, byte background_color);
