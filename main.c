﻿#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include "console.h"
#include "chess.h"

int main() {
  Chess game;
  start_game(&game);

  // DEBUG: Set Console Cursor Position to the bottom of the window just to get the debug console text out of the way during dev.
  COORD pos = {0, 60};
  SetConsoleCursorPosition(hConsoleOut, pos);
}