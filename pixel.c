#include <stdio.h>
#include <Windows.h>
#include "pixel.h"
#include "console.h"

// DEBUG: This is used to see all of the color options available.
void print_all_colors() {
  init_console();

  for (size_t i = 0; i < 256; i++) {
    SetConsoleTextAttribute(hConsoleOut, i);
    wprintf(L"%3d  %s %c\n", i, L"COLOR TEST", PIXEL_FULL_BLOCK);
  }
}

void print_newline() {
  wprintf(L"\n"); 
}

void print_pixel(wint_t pixel) { 
  wprintf(L"%c", pixel); 
}

void print_sprite(Sprite* sprite, COORD screen_offset) {
  byte color;
  COORD screen_position = screen_offset;

  for (size_t i = 0; i < sprite->total_pixels; i++) {
    screen_position.X = screen_offset.X + sprite->pixels[i].position.X;
    screen_position.Y = screen_offset.Y + sprite->pixels[i].position.Y;

    // TODO:  Console Cursor Position does not need to be set if prev.Y == current.Y and prev.X == current.X - 1
    //        The console cursor will always automatically advance whenever a character is printed. If we're printing two
    //        sequential characters in a row then we can skip the call to SetConsoleCursorPosition(). This should optimize
    //        the chess board printing sequence a little.
    SetConsoleCursorPosition(hConsoleOut, screen_position);

    if (sprite->unique_pixel_colors) {
      color = get_color(sprite->pixels[i].foreground_color, sprite->pixels[i].background_color);
    } else {
      color = get_color(sprite->foreground_color, sprite->background_color);
    }

    SetConsoleTextAttribute(hConsoleOut, color);
    print_pixel(sprite->pixels[i].pixel_char);
  }

  // Reset text color.
  SetConsoleTextAttribute(hConsoleOut, kLightGray);
}

// This is used to init a Sprite using parameters.
// This can be used to create a Sprite using the same values for every Pixel.
void init_generic_sprite(Sprite* sprite, wint_t pixel_char, byte foreground_color, byte background_color) {
  Pixel* pixels = malloc(sizeof(Pixel) * sprite->total_pixels);

  for (size_t i = 0; i < sprite->total_pixels; i++) {
    pixels[i].pixel_char = pixel_char;
    pixels[i].foreground_color = foreground_color;
    pixels[i].background_color = background_color;
    pixels[i].position.X = i % sprite->width;
    pixels[i].position.Y = i / sprite->width;
  }

  sprite->pixels = pixels;
}

// This is used to init a Sprite using a static array of Pixels.
// This can be used to create a Sprite where each Pixel value is unique.
// This allows for various colors and shapes in a Sprite.
void init_detailed_sprite(Sprite* sprite, Pixel pixel_arr[]) {
  Pixel* pixels = malloc(sizeof(Pixel) * sprite->total_pixels);

  for (size_t i = 0; i < sprite->total_pixels; i++) {
    pixels[i] = pixel_arr[i];
  }

  sprite->pixels = pixels;
}

void destroy_sprite(Sprite sprite) {
    free(sprite.pixels);
}

byte get_color(byte foreground_color, byte background_color) {
  return (background_color << 4) | foreground_color;
}