#include <Windows.h>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include "console.h"

void init_handle() { 
  hConsoleOut = GetStdHandle(STD_OUTPUT_HANDLE);
  hConsoleIn = GetStdHandle(STD_INPUT_HANDLE);
}

// This is needed to set the console to the correct mode to print unicode characters which will represent a pixel.
void init_console() {
  init_handle();
  SetConsoleMode(hConsoleIn, ENABLE_EXTENDED_FLAGS | ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT);

  // TODO: Was trying to get rid of the scroll bar here... wasn't succesful. Maybe I'll revisit this later.
  //       I also should really enforce the screen size/resolution here as well.

  //CONSOLE_SCREEN_BUFFER_INFO  lpConsoleScreenBufferInfo;
  //GetConsoleScreenBufferInfo(hConsole, &lpConsoleScreenBufferInfo);
  //COORD size = {lpConsoleScreenBufferInfo.dwSize.X - 2, lpConsoleScreenBufferInfo.dwSize.Y};
  //SetConsoleScreenBufferSize(hConsole, size);

  _setmode(_fileno(stdout), _O_U8TEXT);
}