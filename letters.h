#pragma once
#include "pixel.h"

#define TOTAL_LETTER_A_PIXELS 14
#define LETTER_WIDTH 10
#define LETTER_HEIGHT 5

Sprite letter_a;
Pixel letter_a_pixels[];

void init_letters();