﻿#include <stdbool.h>
#include <stdlib.h>
#include "console.h"
#include "pixel.h"
#include "chess.h"
#include "mouse_event_controller.h"
#include "move_controller.h"

Sprite empty_sprite = {0, 0, 0, false, 0, 0};
Sprite square_sprite = {SQUARE_WIDTH * SQUARE_HEIGHT, SQUARE_WIDTH, SQUARE_HEIGHT, false, 0, 0};
Sprite circle_sprite = {TOTAL_CIRCLE_PIXELS, CIRCLE_WIDTH, CIRCLE_HEIGHT, false, kBlue, kBlue};

Sprite pawn_sprite = {TOTAL_PAWN_PIXELS, SQUARE_WIDTH, SQUARE_HEIGHT, false, 0, 0};
Sprite rook_sprite = {TOTAL_ROOK_PIXELS, SQUARE_WIDTH, SQUARE_HEIGHT, false, 0, 0};
Sprite knight_sprite = {TOTAL_KNIGHT_PIXELS, SQUARE_WIDTH, SQUARE_HEIGHT, false, 0, 0};
Sprite bishop_sprite = {TOTAL_BISHOP_PIXELS, SQUARE_WIDTH, SQUARE_HEIGHT, false, 0, 0};
Sprite queen_sprite = {TOTAL_QUEEN_PIXELS, SQUARE_WIDTH, SQUARE_HEIGHT, false, 0, 0};
Sprite king_sprite = {TOTAL_KING_PIXELS, SQUARE_WIDTH, SQUARE_HEIGHT, false, 0, 0};

Pixel pawn_pixels[] = {
  {{6, 1}, PIXEL_LOWER_HALF, 0, 0}, 
  {{7, 1}, PIXEL_LOWER_HALF, 0, 0},
  {{5, 2}, PIXEL_RIGHT_HALF, 0, 0}, 
  {{6, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 2}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{8, 2}, PIXEL_LEFT_HALF, 0, 0},
  {{6, 3}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{7, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 4}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{6, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 4}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{8, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 5}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{5, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 5}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{7, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 5}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{9, 5}, PIXEL_FULL_BLOCK, 0, 0}
};

Pixel rook_pixels[] = {
  {{3, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{4, 1}, PIXEL_RIGHT_HALF, 0, 0},  
  {{5, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{6, 1}, PIXEL_RIGHT_HALF, 0, 0},  
  {{7, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{8, 1}, PIXEL_RIGHT_HALF, 0, 0},  
  {{9, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{10, 1}, PIXEL_RIGHT_HALF, 0, 0}, 
  {{3, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 2}, PIXEL_FULL_BLOCK, 0, 0},  
  {{5, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 2}, PIXEL_FULL_BLOCK, 0, 0},  
  {{7, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 2}, PIXEL_FULL_BLOCK, 0, 0},  
  {{9, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{10, 2}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{3, 3}, PIXEL_FULL_BLOCK, 0, 0},  
  {{4, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 3}, PIXEL_FULL_BLOCK, 0, 0},  
  {{6, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 3}, PIXEL_FULL_BLOCK, 0, 0},  
  {{8, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 3}, PIXEL_FULL_BLOCK, 0, 0},  
  {{10, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{3, 4}, PIXEL_FULL_BLOCK, 0, 0},  
  {{4, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 4}, PIXEL_FULL_BLOCK, 0, 0},  
  {{6, 4}, PIXEL_UPPER_HALF, 0, 0},
  {{7, 4}, PIXEL_UPPER_HALF, 0, 0},  
  {{8, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 4}, PIXEL_FULL_BLOCK, 0, 0},  
  {{10, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{3, 5}, PIXEL_RIGHT_HALF, 0, 0},  
  {{4, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 5}, PIXEL_LEFT_HALF, 0, 0},   
  {{8, 5}, PIXEL_RIGHT_HALF, 0, 0},
  {{9, 5}, PIXEL_FULL_BLOCK, 0, 0},  
  {{10, 5}, PIXEL_LEFT_HALF, 0, 0},
  {{3, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 6}, PIXEL_FULL_BLOCK, 0, 0},  
  {{5, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 6}, PIXEL_FULL_BLOCK, 0, 0},  
  {{7, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 6}, PIXEL_FULL_BLOCK, 0, 0},  
  {{9, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{10, 6}, PIXEL_FULL_BLOCK, 0, 0}
};

Pixel knight_pixels[] = {
  {{5, 0}, PIXEL_LOWER_HALF, 0, 0},
  {{7, 0}, PIXEL_LOWER_HALF, 0, 0},
  {{2, 1}, PIXEL_LOWER_HALF, 0, 0},
  {{3, 1}, PIXEL_FULL_BLOCK, 0, 0},  
  {{4, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{2, 2}, PIXEL_UPPER_HALF, 0, 0},
  {{3, 2}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{4, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 2}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{6, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 2}, PIXEL_LOWER_HALF, 0, 0},
  {{8, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{10, 2}, PIXEL_LOWER_HALF, 0, 0},
  {{2, 3}, PIXEL_UPPER_HALF, 0, 0},
  {{3, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{10, 3}, PIXEL_LOWER_HALF, 0, 0},
  {{11, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 4}, PIXEL_RIGHT_HALF, 0, 0},
  {{5, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 4}, PIXEL_LEFT_HALF, 0, 0},
  {{9, 4}, PIXEL_UPPER_HALF, 0, 0},
  {{10, 4}, PIXEL_UPPER_HALF, 0, 0},
  {{4, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 5}, PIXEL_LOWER_HALF, 0, 0},
  {{3, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{10, 6}, PIXEL_FULL_BLOCK, 0, 0}
};

Pixel bishop_pixels[] = {
  {{6, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{7, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 2}, PIXEL_LOWER_HALF, 0, 0},
  {{5, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 2}, PIXEL_RIGHT_HALF, 0, 0},
  {{8, 2}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{9, 2}, PIXEL_LOWER_HALF, 0, 0},
  {{4, 3}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{5, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 3}, PIXEL_FULL_BLOCK, 0, 0}, 
  {{9, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 5}, PIXEL_UPPER_HALF, 0, 0}, 
  {{5, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 5}, PIXEL_UPPER_HALF, 0, 0},
  {{3, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{10, 6}, PIXEL_FULL_BLOCK, 0, 0}
};

Pixel queen_pixels[] = {
  {{6, 0}, PIXEL_LOWER_HALF, 0, 0},
  {{7, 0}, PIXEL_LOWER_HALF, 0, 0},
  {{3, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{4, 1}, PIXEL_RIGHT_HALF, 0, 0},
  {{5, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{6, 1}, PIXEL_UPPER_HALF, 0, 0},
  {{7, 1}, PIXEL_UPPER_HALF, 0, 0},
  {{8, 1}, PIXEL_RIGHT_HALF, 0, 0},
  {{9, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{10, 1}, PIXEL_RIGHT_HALF, 0, 0},
  {{3, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{10, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 4}, PIXEL_RIGHT_HALF, 0, 0},
  {{5, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 4}, PIXEL_LEFT_HALF, 0, 0},
  {{4, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{3, 6}, PIXEL_RIGHT_HALF, 0, 0},
  {{4, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{10, 6}, PIXEL_LEFT_HALF, 0, 0}
};

Pixel king_pixels[] = {
  {{5, 0}, PIXEL_LOWER_HALF, 0, 0},
  {{6, 0}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 0}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 0}, PIXEL_LOWER_HALF, 0, 0},
  {{3, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 1}, PIXEL_LEFT_HALF, 0, 0},
  {{5, 1}, PIXEL_UPPER_HALF, 0, 0},
  {{6, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 1}, PIXEL_UPPER_HALF, 0, 0},
  {{9, 1}, PIXEL_RIGHT_HALF, 0, 0},
  {{10, 1}, PIXEL_FULL_BLOCK, 0, 0},
  {{3, 2}, PIXEL_UPPER_HALF, 0, 0},
  {{4, 2}, PIXEL_LEFT_HALF, 0, 0},
  {{6, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 2}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 2}, PIXEL_RIGHT_HALF, 0, 0},
  {{10, 2}, PIXEL_UPPER_HALF, 0, 0},
  {{4, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 3}, PIXEL_FULL_BLOCK, 0, 0},
  {{4, 4}, PIXEL_RIGHT_HALF, 0, 0},
  {{5, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 4}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 4}, PIXEL_LEFT_HALF, 0, 0},
  {{4, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 5}, PIXEL_FULL_BLOCK, 0, 0},
  {{3, 6}, PIXEL_RIGHT_HALF, 0, 0},
  {{4, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{5, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{6, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{7, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{8, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{9, 6}, PIXEL_FULL_BLOCK, 0, 0},
  {{10, 6}, PIXEL_LEFT_HALF, 0, 0}
};

Pixel circle_pixels[] = {
  {{6, 1}, PIXEL_LOWER_HALF,  0, 0},
  {{7, 1}, PIXEL_LOWER_HALF,  0, 0},
  {{5, 2}, PIXEL_RIGHT_HALF,  0, 0},
  {{8, 2}, PIXEL_LEFT_HALF,  0, 0},
  {{4, 3}, PIXEL_RIGHT_HALF,  0, 0},
  {{9, 3}, PIXEL_LEFT_HALF,  0, 0},
  {{5, 4}, PIXEL_RIGHT_HALF,  0, 0},
  {{8, 4}, PIXEL_LEFT_HALF,  0, 0},
  {{6, 5}, PIXEL_UPPER_HALF,  0, 0},
  {{7, 5}, PIXEL_UPPER_HALF,  0, 0},
};

void init_sprites() {
  init_generic_sprite(&square_sprite, PIXEL_FULL_BLOCK, 0, 0);

  init_detailed_sprite(&pawn_sprite, pawn_pixels);
  init_detailed_sprite(&rook_sprite, rook_pixels);
  init_detailed_sprite(&bishop_sprite, bishop_pixels);
  init_detailed_sprite(&knight_sprite, knight_pixels);
  init_detailed_sprite(&queen_sprite, queen_pixels);
  init_detailed_sprite(&king_sprite, king_pixels);

  init_detailed_sprite(&circle_sprite, circle_pixels);
}

void init_board(Board* board) {
  bool checkered_pattern = true;
  unsigned int current_column = 0;
  unsigned int current_row = 0;
  Square* current_square;
  Piece* current_piece;

  for (size_t i = 0; i < TOTAL_SQUARES; i++) {
    current_column = i / SQUARES_PER_COLUMN;
    current_row = i % SQUARES_PER_ROW;
    current_square = &board->square[current_column][current_row];
    current_piece = &current_square->piece;

    // Set the square position relative to its row and column. (Also known as rank and file in chess)
    current_square->position.column = current_column;
    current_square->position.row = current_row;

    // Create a checkered pattern on the board.
    current_square->color = checkered_pattern ? kLight : kDark;
    if (current_row != 7) {
      checkered_pattern = !checkered_pattern;
    }

    // Set the relative color for every piece.
    if (current_column == 0 || current_column == 1) {
      current_piece->color = kDark;
    } else if (current_column == 6 || current_column == 7) {
      current_piece->color = kLight;
    } else {
      current_piece->color = kNone;
    }

    // Set the relative type and sprite for every piece or empty square.
    if (current_column == 1 || current_column == 6) {
      current_piece->type = kPawn;
      current_piece->sprite = pawn_sprite;
    } else if (current_column == 0 || current_column == 7) {
      if (current_row == 0 || current_row == 7) {
        current_piece->type = kRook;
        current_piece->sprite = rook_sprite;
      } else if (current_row == 1 || current_row == 6) {
        current_piece->type = kKnight;
        current_piece->sprite = knight_sprite;
      } else if (current_row == 2 || current_row == 5) {
        current_piece->type = kBishop;
        current_piece->sprite = bishop_sprite;
      } else if (current_row == 3) {
        current_piece->type = kQueen;
        current_piece->sprite = queen_sprite;
      } else if (current_row == 4) {
        current_piece->type = kKing;
        current_piece->sprite = king_sprite;
      }
    } else {
      current_piece->type = kEmpty;
      current_piece->sprite = empty_sprite;
    }
  }
}

typedef void(*fp_mouse_callback)(Board*, Color, Square*, COORD*);

void turn_handler(Board* board, Color current_turn, Square* selected_square, Square prev_selected_square);
void get_square_from_mouse_click(Board* board, Color current_turn, Square* square, COORD* mouse_click_coord);
COORD screen_to_board(COORD screen_position);

void start_game(Chess* game) { 
  Board board;
  Square* selected_square = malloc(sizeof(Square));
  printing_moves = true;

  init_sprites();
  init_board(&board);
  init_console();

  game->board = board;
  game->turn = kLight;

  game->player_light.name = L"White";
  game->player_dark.name = L"Black";

  print_board(&board);

  // TODO: Add a quit feature.
  while (true) {
    mouse_handler(&board, game->turn, selected_square, get_square_from_mouse_click);
  }
}

void get_square_from_mouse_click(Board* board, Color turn, Square* square, COORD* mouse_click_coord) {
  Square prev_selected_square = *square;
  COORD pos = screen_to_board(*mouse_click_coord);
  *square = board->square[pos.Y][pos.X];

  turn_handler(board, turn, square, prev_selected_square);
}

// Does board need to be a pointer? It's not being changed so I don't think so.
void print_board(Board* board) {
  COORD center_of_console = {((WINDOW_WIDTH - PIXELS_PER_ROW) / 2), ((WINDOW_HEIGHT - PIXELS_PER_COL) / 2)};
  COORD position = center_of_console;
  unsigned int current_column;
  unsigned int current_row;
  Square current_square;
  Piece current_piece;

  for (size_t i = 0; i < TOTAL_SQUARES; i++) {
    current_column = i % SQUARES_PER_COLUMN;
    current_row = i / SQUARES_PER_ROW;
    current_square = board->square[current_row][current_column];
    current_piece = current_square.piece;

    // Set the square sprite color and print it.
    square_sprite.foreground_color = current_square.color == kLight ? LIGHT_SQUARE_COLOR : DARK_SQUARE_COLOR;
    square_sprite.background_color = current_square.color == kLight ? LIGHT_SQUARE_COLOR : DARK_SQUARE_COLOR;
    print_sprite(&square_sprite, position);

    // Set the piece sprite color and print it.
    if (current_square.piece.type != kEmpty) {
      current_piece.sprite.foreground_color = current_piece.color == kLight ? LIGHT_PIECE_COLOR : DARK_PIECE_COLOR;
      current_piece.sprite.background_color = current_square.color == kLight ? LIGHT_SQUARE_COLOR : DARK_SQUARE_COLOR;
      print_sprite(&current_piece.sprite, position);
    }

    // This is true every time the index is a multiple of 8.
    // This is how I determine when to increase the Y position to print the next row.
    if (i != 0 && (i + 1) % SQUARES_PER_ROW == 0) {
      position.X = center_of_console.X;
      position.Y += SQUARE_HEIGHT;
    } else {
      position.X += SQUARE_WIDTH;
    }
  }
}

// TODO: Nearly identical to print_board... that is very bad.
//       Fix this to be more versatile. 
//       Consolidate similar functions print_board and refresh_board.
void refresh_board(Board board, Square* squares_to_refresh, unsigned int num_of_sqr_to_refresh) {
  Square current_square;
  Piece current_piece;
  COORD position;

  for (size_t i = 0; i < num_of_sqr_to_refresh; i++) {
    position.X = ((squares_to_refresh[i].position.column * SQUARE_WIDTH) + BOARD_LEFTMOST);
    position.Y = ((squares_to_refresh[i].position.row * SQUARE_HEIGHT) + BOARD_TOPMOST);
    current_square = board.square[squares_to_refresh[i].position.row][squares_to_refresh[i].position.column];
    current_piece = current_square.piece;
    
    // Set the square sprite color and print it.
    square_sprite.foreground_color = current_square.color == kLight ? LIGHT_SQUARE_COLOR : DARK_SQUARE_COLOR;
    square_sprite.background_color = current_square.color == kLight ? LIGHT_SQUARE_COLOR : DARK_SQUARE_COLOR;
    print_sprite(&square_sprite, position);

    // Set the piece sprite color and print it.
    if (current_square.piece.type != kEmpty) {
      current_piece.sprite.foreground_color = current_piece.color == kLight ? LIGHT_PIECE_COLOR : DARK_PIECE_COLOR;
      current_piece.sprite.background_color = current_square.color == kLight ? LIGHT_SQUARE_COLOR : DARK_SQUARE_COLOR;
      print_sprite(&current_piece.sprite, position);
    }
  }
}

void turn_handler(Board* board, Color current_turn, Square* selected_square, Square prev_selected_square) {
  if (selected_square->piece.color == current_turn) {
    // TODO: Remove print_board from here and replace with a more effecient refresh_board after refactor is complete.
    print_board(board);
    reset_num_of_valid_moves();
    generate_valid_moves(board, *selected_square);
    if (printing_moves) {
      print_valid_moves();
    }
  } else if (*num_of_valid_moves && valid_move_found(*selected_square)) {
    move_piece(board, prev_selected_square, *selected_square);
    // TODO: Remove print_board and replace with refresh_board after refactor is complete.
    //refresh_board(*board, valid_moves, *num_of_valid_moves);
    print_board(board);
  } else {
    reset_num_of_valid_moves();
  }
}

COORD screen_to_board(COORD screen_position) {
  COORD board_position = {(screen_position.X - BOARD_LEFTMOST) / SQUARE_WIDTH, (screen_position.Y - BOARD_TOPMOST) / SQUARE_HEIGHT};
  return board_position;
}